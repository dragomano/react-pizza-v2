import Header from "../components/Header.jsx";
import { Outlet } from "react-router-dom";
import { Suspense } from "react";

function MainLayout() {
  return (
    <div className="wrapper">
      <Header />
      <div className="content">
        <Suspense
          fallback={
            <div className="container">
              <h2 className="content__title">Загрузка...</h2>
            </div>
          }
        >
          <Outlet />
        </Suspense>
      </div>
    </div>
  );
}

export default MainLayout;
