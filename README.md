# React Pizza v2

Реализация [популярного учебного проекта](https://www.youtube.com/playlist?list=PL0FGkDGJQjJG9eI85xM1_iLIf6BcEdaNl)

## Отличия от оригинала

- Сборщик `Vite` вместо `Webpack`
- Нет типизации Typescript
- [MobX](https://mobx.js.org/react-integration.html) вместо React Toolkit
- Кэширование данных с помощью [react-query](https://github.com/TanStack/query) и [mobx-persist-store](https://github.com/quarrant/mobx-persist-store)
- Пользовательские хуки
- Использование встроенного метода `URLSearchParams` вместо библиотеки `qs`
- Подправлена пагинация
- Использование API https://mokky.dev вместо https://mockapi.io
- Использование утилиты [AutoAnimate](https://auto-animate.formkit.com) для анимации блоков пицц

## Установка и запуск

`pnpm i`

`pnpm run dev`
