import filterStore from "../stores/filterStore.js";
import { observer } from "mobx-react-lite";
import useCategories from "../hooks/useCategories.jsx";

const Categories = observer(() => {
  const { categoryId, setCategoryId, setCurrentPage } = filterStore;
  const { isLoading, error, data: categories } = useCategories();

  const onChangeCategory = (i) => {
    setCategoryId(i);
    setCurrentPage(1);
  };

  if (isLoading)
    return (
      <div className="categories">
        <ul>
          <li>Загрузка...</li>
        </ul>
      </div>
    );

  if (error) {
    console.error("Произошла ошибка при получении данных");
  }

  return (
    <div className="categories">
      <ul>
        {categories.map((category) => (
          <li
            key={category.id}
            onClick={() => onChangeCategory(category.id)}
            className={categoryId === category.id ? "active" : undefined}
          >
            {category.title}
          </li>
        ))}
      </ul>
    </div>
  );
});

export default Categories;
