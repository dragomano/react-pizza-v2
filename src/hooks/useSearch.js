import filterStore from "../stores/filterStore.js";
import debounce from "lodash.debounce";
import { useEffect, useRef, useState } from "react";

const useSearch = () => {
  const { setSearchValue } = filterStore;
  const inputRef = useRef(null);
  const [value, setValue] = useState("");

  const onClickClear = () => {
    setValue("");
    setSearchValue("");
    inputRef.current.focus();
  };

  const debouncedSearch = useRef(
    debounce((str) => setSearchValue(str), 300),
  ).current;

  const onChangeInput = (event) => {
    setValue(event.target.value);
    debouncedSearch(event.target.value);
  };

  useEffect(() => {
    return () => {
      debouncedSearch.cancel();
    };
  }, [debouncedSearch]);

  return {
    inputRef,
    value,
    onChangeInput,
    onClickClear,
  };
};

export default useSearch;
