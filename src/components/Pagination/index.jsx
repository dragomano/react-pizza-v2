import ReactPaginate from "react-paginate";
import styles from "./Pagination.module.scss";

const Pagination = ({ total, current, limit, onPageChange }) => {
  return (
    <ReactPaginate
      className={styles.root}
      breakLabel="..."
      nextLabel=">"
      previousLabel="<"
      forcePage={current - 1}
      pageCount={total}
      pageRangeDisplayed={limit}
      onPageChange={(event) => onPageChange(event.selected + 1)}
      renderOnZeroPageCount={null}
    />
  );
};

export default Pagination;
