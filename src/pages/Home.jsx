import Categories from "../components/Categories.jsx";
import Sort from "../components/Sort.jsx";
import Pagination from "../components/Pagination/index.jsx";
import filterStore from "../stores/filterStore.js";
import pizzaStore from "../stores/pizzaStore.js";
import { observer } from "mobx-react-lite";
import useNavigation from "../hooks/useNavigation.js";
import usePizzas from "../hooks/usePizzas.jsx";
import useCategories from "../hooks/useCategories.jsx";
import PizzaBlock from "../components/PizzaBlock/index.jsx";
import Skeleton from "../components/PizzaBlock/Skeleton.jsx";
import { useAutoAnimate } from "@formkit/auto-animate/react";

const Home = observer(() => {
  const { searchValue, categoryId, currentPage, setCurrentPage } = filterStore;
  const { items, totalPages, limit } = pizzaStore;
  const { data: categories } = useCategories();
  const { isLoading, error, data } = usePizzas();
  const [parent, enableAnimations] = useAutoAnimate();

  useNavigation();

  const handlePageClick = (number) => {
    setCurrentPage(number);
  };

  const filteredPizzas = (isLoading ? items : data).filter((obj) =>
    obj.title.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()),
  );

  const pizzas = filteredPizzas.map((pizza) => (
    <PizzaBlock key={pizza.id} {...pizza} />
  ));

  const skeletons = [...new Array(4)].map((_, index) => (
    <Skeleton key={index} />
  ));

  return (
    <div className="container">
      <div className="content__top">
        <Categories />
        <Sort />
      </div>
      <h2 className="content__title">
        {(categories && categories[categoryId].title) ?? "Все пиццы"}
      </h2>
      <div className="content__items" ref={parent}>
        {error ? (
          <div className="content__error-info">
            <h2>Произошла ошибка</h2>
            <p>К сожалению, не удалось получить список пицц!</p>
          </div>
        ) : isLoading ? (
          skeletons
        ) : (
          pizzas
        )}
      </div>
      {!searchValue && (
        <Pagination
          total={totalPages}
          current={currentPage}
          limit={limit}
          onPageChange={handlePageClick}
        />
      )}
    </div>
  );
});

export default Home;
