import { useQuery } from "react-query";
import axios from "axios";

const useCategories = () => {
  return useQuery(
    ["categories"],
    async () => {
      const { data } = await axios.get(
        "https://2adbdc40a21d6181.mokky.dev/categories",
      );

      return data;
    },
    {
      staleTime: 60 * 60 * 1000, // Кэш будет храниться 60 минут
    },
  );
};

export default useCategories;
