import "./scss/app.scss";
import { Route, Routes } from "react-router-dom";
import { lazy } from "react";
import MainLayout from "./layouts/MainLayout.jsx";
import { QueryClient, QueryClientProvider } from "react-query";

const Home = lazy(() => import("./pages/Home.jsx"));
const Cart = lazy(() => import("./pages/Cart.jsx"));
const FullPizza = lazy(() => import("./pages/FullPizza.jsx"));
const NotFound = lazy(() => import("./pages/NotFound.jsx"));

const queryClient = new QueryClient();

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route path="/" element={<Home />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/pizza/:id" element={<FullPizza />} />
          <Route path="*" element={<NotFound />} />
        </Route>
      </Routes>
    </QueryClientProvider>
  );
};

export default App;
