import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import axios from "axios";

const usePizza = () => {
  const { id } = useParams();

  return useQuery(
    ["pizza", id],
    async () => {
      const { data } = await axios.get(
        "https://2adbdc40a21d6181.mokky.dev/items/" + id,
      );

      return data;
    },
    {
      staleTime: 5 * 60 * 1000, // Кэш будет храниться 5 минут
    },
  );
};

export default usePizza;
