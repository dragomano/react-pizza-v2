import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

class CartStore {
  _items = [];

  constructor() {
    makeAutoObservable(this);

    makePersistable(this, {
      name: "CartStore",
      properties: ["_items"],
      storage: window.localStorage,
    }).catch((error) =>
      console.warn("Ошибка синхронизации с localStorage", error),
    );
  }

  get items() {
    return this._items;
  }

  addItem = (item) => {
    const findItem = this._items.find((obj) => obj.id === item.id);

    if (findItem) {
      findItem.count++;
    } else {
      this._items = [
        ...this._items,
        {
          ...item,
          count: 1,
        },
      ];
    }
  };

  removeItem = (id) => {
    this._items = this._items.filter((obj) => obj.id !== id);
  };

  clearItems = () => {
    this._items = [];
  };

  minusItem = (id) => {
    const findItem = this._items.find((obj) => obj.id === id);

    if (findItem) findItem.count--;
  };

  get totalPrice() {
    return this._items.reduce((sum, obj) => {
      return obj.price * obj.count + sum;
    }, 0);
  }

  get totalCount() {
    return this._items.reduce((sum, item) => sum + item.count, 0);
  }
}

const cartStore = new CartStore();

export default cartStore;
