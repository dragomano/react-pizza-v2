import { makeAutoObservable } from "mobx";

class FilterStore {
  _searchValue = "";
  _categoryId = 0;
  _currentPage = 1;
  _sort = {
    name: "популярности (DESC)",
    property: "rating",
  };

  constructor() {
    makeAutoObservable(this);
  }

  get searchValue() {
    return this._searchValue;
  }

  get categoryId() {
    return this._categoryId;
  }

  get currentPage() {
    return this._currentPage;
  }

  get sort() {
    return this._sort;
  }

  setSearchValue = (value) => {
    this._searchValue = value;
  };

  setCategoryId = (id) => {
    this._categoryId = +id;
  };

  setCurrentPage = (page) => {
    this._currentPage = +page;
  };

  setSortType = (sort) => {
    this._sort = sort;
  };

  setFilters = (filter) => {
    this._categoryId = +filter.category;
    this._currentPage = +filter.page;
    this._sort = filter.sort;
  };
}

const filterStore = new FilterStore();

export default filterStore;
