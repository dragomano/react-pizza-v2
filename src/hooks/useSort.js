import { useEffect, useRef, useState } from "react";

const useSort = () => {
  const [open, setOpen] = useState(false);
  const sortRef = useRef();

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!event.composedPath().includes(sortRef.current)) {
        setOpen(false);
      }
    };

    document.body.addEventListener("click", handleClickOutside);
    return () => document.body.removeEventListener("click", handleClickOutside);
  }, []);

  return {
    open,
    setOpen,
    sortRef,
  };
};

export default useSort;
