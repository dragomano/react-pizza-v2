import { Link, useNavigate } from "react-router-dom";
import usePizza from "../hooks/usePizza.jsx";

const FullPizza = () => {
  const navigate = useNavigate();
  const { isLoading, error, data: pizza } = usePizza();

  if (isLoading)
    return (
      <div className="container">
        <h2 className="content__title">Загрузка...</h2>
      </div>
    );

  if (error) {
    alert("Произошла ошибка при получении данных");
    navigate("/");
  }

  if (!pizza)
    return (
      <div className="container">
        <h2 className="content__title">
          К сожалению, данных о запрашиваемой пицце нет...
        </h2>
      </div>
    );

  return (
    <div className="container" style={{ textAlign: "center" }}>
      <img alt={pizza.title} src={pizza.imageUrl} />
      <div className="pizza-details">
        <h2>{pizza.title}</h2>
        <div className="pizza-block__price">от {pizza.price} ₽</div>
        <Link to="/">
          <button className="button button--outline button--add">
            <span>Назад</span>
          </button>
        </Link>
      </div>
    </div>
  );
};

export default FullPizza;
