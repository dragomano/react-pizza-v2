import { useNavigate } from "react-router-dom";
import { useEffect, useRef } from "react";
import filterStore from "../stores/filterStore.js";
import { sortList } from "../components/Sort.jsx";

const useNavigation = () => {
  const { categoryId, currentPage, sort, setFilters } = filterStore;
  const navigate = useNavigate();
  const isMounted = useRef(false);

  useEffect(() => {
    if (window.location.search) {
      const searchParams = new URLSearchParams(
        window.location.search.substring(1),
      );
      const params = Object.fromEntries(searchParams);
      const sort = sortList.find((obj) => obj.property === params.sort);

      setFilters({ ...params, sort });
    }
  }, []);

  useEffect(() => {
    if (isMounted.current) {
      const queryString = new URLSearchParams({
        category: categoryId,
        sort: sort.property,
        page: currentPage,
      });

      navigate(`?${queryString.toString()}`);
    }

    isMounted.current = true;
  }, [categoryId, sort.property, currentPage]);
};

export default useNavigation;
