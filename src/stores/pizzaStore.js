import { makeAutoObservable } from "mobx";

class PizzaStore {
  _items = [];
  _totalPages = 1;
  _limit = 4;

  constructor() {
    makeAutoObservable(this);
  }

  get items() {
    return this._items;
  }

  get totalPages() {
    return this._totalPages;
  }

  get limit() {
    return this._limit;
  }

  setItems = (items) => {
    this._items = items;
  };

  setTotalPages = (totalPages) => {
    this._totalPages = totalPages;
  };

  setLimit = (limit) => {
    this._limit = limit;
  };
}

const pizzaStore = new PizzaStore();

export default pizzaStore;
