import filterStore from "../stores/filterStore.js";
import pizzaStore from "../stores/pizzaStore.js";
import axios from "axios";
import { useQuery } from "react-query";

const usePizzas = () => {
  const { categoryId, currentPage: page, sort } = filterStore;
  const { limit, setItems, setTotalPages, setLimit } = pizzaStore;
  const category = categoryId > 0 ? categoryId : null;
  const sortBy = sort.property;

  return useQuery(["pizzas", category, sortBy, page, limit], async () => {
    const { data } = await axios.get(
      "https://2adbdc40a21d6181.mokky.dev/items",
      {
        params: {
          category,
          sortBy,
          page,
          limit,
        },
      },
    );

    setItems(data.items);
    setTotalPages(data.meta["total_pages"]);
    setLimit(data.meta["per_page"]);

    return data.items;
  });
};

export default usePizzas;
